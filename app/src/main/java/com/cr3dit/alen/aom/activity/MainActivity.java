package com.cr3dit.alen.aom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.fragments.BasketFragment;
import com.cr3dit.alen.aom.fragments.MainFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    
    private Drawer mResult;
    private SharedPreferences mAccountPref;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar;

        //adding first fragment to R:id.MainFrame placeholder if activity has never existed
        if (savedInstanceState == null) 
            {
            getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.MainFrame, MainFragment.newInstance(), "Main Fragment")
                .commit();
            }

        mAccountPref = getApplicationContext().getSharedPreferences("MyPref", 0);

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        
        //creating Drawer
        createDrawer(myToolbar, mAccountPref.getBoolean("isLoggedIn",false));
    }


    public void createDrawer(Toolbar myToolbar, final boolean loggedIn)
        {
        //Library guide url https://github.com/mikepenz/MaterialDrawer
        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
            .withActivity(this)
            .withHeaderBackground(R.drawable.drawerbg)
            .addProfiles(
                    new ProfileDrawerItem().withName(mAccountPref.getString("name","")).withEmail(mAccountPref.getString("email","")).withIcon(mAccountPref.getString("photoUrl",""))
            ).build();

            //add drawer items
        final PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Home");
        SecondaryDrawerItem logStat;
        SecondaryDrawerItem settings=new SecondaryDrawerItem().withIdentifier(3).withName("Settings");
        SecondaryDrawerItem exit=new SecondaryDrawerItem().withIdentifier(4).withName("Exit");

        if (loggedIn)
            {
            logStat = new SecondaryDrawerItem().withIdentifier(2).withName("Log out");
            }            
        else
            {
            logStat = new SecondaryDrawerItem().withIdentifier(2).withName("Log in");
            }

        //create the drawer and remember the `Drawer` mResult object
         mResult = new DrawerBuilder()
            .withActivity(this).withAccountHeader(headerResult)
            .withToolbar(myToolbar)
            .addDrawerItems(
                    item1,
                    new DividerDrawerItem(),settings,logStat,new DividerDrawerItem(),exit)
            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                @Override
                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                  if(drawerItem.getIdentifier()==1)
                      {
                          finish();
                          startActivity(getIntent());
                          mResult.closeDrawer();
                      }
                  else if(drawerItem.getIdentifier()==2)
                      {
                        if(loggedIn) 
                            {
                            startActivity(new Intent(getApplicationContext(), LogoutActivity.class));
                            mResult.closeDrawer();
                            }
                        else
                            startActivity(new Intent(getApplicationContext(), LoginActivity.class).putExtra("fromActivity",true));
                            mResult.closeDrawer();
                       }
                  else if(drawerItem.getIdentifier()==3)
                       {
                        //todo go to settings activity
                        mResult.closeDrawer();
                        }
                    return true;
                }
            }).build();
            
        //initialize and create the image loader logic
        DrawerImageLoader.init(new AbstractDrawerImageLoader() {
            @Override
            public void set(ImageView imageView, Uri uri, Drawable placeholder) 
                {
                Picasso.with(imageView.getContext()).load(uri).placeholder(placeholder).into(imageView);
                }

                @Override
                public void cancel(ImageView imageView) 
                {
                    Picasso.with(imageView.getContext()).cancelRequest(imageView);
                }

            });
            mResult.closeDrawer();
        }


 @Override
    public boolean onCreateOptionsMenu(Menu menu) 
        {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
        }
    
    
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;

            case R.id.action_basket:
                getSupportFragmentManager()
                        .beginTransaction().addToBackStack(null)
                        .replace(R.id.MainFrame, new BasketFragment(), "BasketFrag")
                        .commit();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
//override back button press
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() >= 0) {
            getFragmentManager().popBackStack();
        } 
        else {
        }
        super.onBackPressed();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
