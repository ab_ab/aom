package com.cr3dit.alen.aom.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cr3dit.alen.aom.model.Article;
import com.cr3dit.alen.aom.model.Category;
import com.cr3dit.alen.aom.model.DbVersionCheck;
import com.cr3dit.alen.aom.model.Entrepreneur;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alen on 12.01.2017..
 */

//aomsys.ddns.net/entrepreneurs
//aomsys.ddns.net/articles
//aomsys.ddns.net/categories
//aomsys.ddns.net/dbinfo


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "aom";

    // database columns
    private static final String KEY_NAME = "article_name";
    private static final String KEY_ID = "_article_id";
    private static final String KEY_ORDER_ID = "_order_id";
    private static final String KEY_ORDER_DETAILS_ID = "_order_details_id";
    private static final String KEY_ARTICLE_PICTURE = "article_picture";

    private static final String KEY_PRICE_1 = "price_1";
    private static final String KEY_PRICE_2 = "price_2";
    private static final String KEY_ENTREPRENEUR_ID = "_entrepreneur_id";
    private static final String KEY_ENTREPRENEUR_NAME = "entrepreneur_name";
    private static final String KEY_ENTREPRENEUR_PICTURE ="entrepreneur_pic";


    private static final String KEY_ENTREPRENEUR_LOCATION = "entrepreneur_location";
    private static final String KEY_ARTICLE_QUANTITY = "article_quantity";
    private static final String KEY_BASKET_QUANTITY = "basket_quantity";
    private static final String KEY_CATEGORY_ID = "_category_id";
    private static final String KEY_CATEGORY_NAME = "category_name";
    private static final String KEY_BASKET_ID="_basket_id";
    private static final String KEY_IS_ORDERED="is_ordered";
    private static final String KEY_ORDER_QUANTITY="order_quantity";
    private static final String KEY_CUSTOMER_ID="_customer_id";

    private static String KEY_DB_VERSION="_db_version";
    private static String KEY_ARTICLE_TABLE_VERSION="articles_version";
    private static String KEY_CATEGORIES_TABLE_VERSION="categories_version";
    private static String KEY_ENTREPRENEURS_TABLE_VERSION="entrepreneurs_version";





    //database tables
    private static final String TABLE_CLIENT_ARTICLES = "articles";
    private static final String TABLE_CLIENT_ENTREPRENEURS = "entrepreneurs";
    private static final String TABLE_CLIENT_BASKET = "basket";
    private static final String TABLE_CLIENT_CATEGORIES = "categories";
    private static final String TABLE_DB_STATUS = "db_status";

    private static final String TABLE_CLIENT_ORDERS="orders";
    private static final String TABLE_CLIENT_ORDER_DETAILS="order_details";



    //create tables
    private static final String CREATE_TABLE_CLIENT_ARTICLES = "CREATE TABLE " + TABLE_CLIENT_ARTICLES + "("
            + KEY_ID + " TEXT PRIMARY KEY," + KEY_NAME + " TEXT," + KEY_ARTICLE_QUANTITY + " REAL," + KEY_PRICE_1 +
            " REAL," + KEY_PRICE_2 + " REAL," + KEY_ENTREPRENEUR_ID + " TEXT," + KEY_CATEGORY_ID + " TEXT," + KEY_ARTICLE_PICTURE+" TEXT" + ")";

    private static final String CREATE_TABLE_CLIENT_ENTREPRENEURS = "CREATE TABLE " + TABLE_CLIENT_ENTREPRENEURS + "("
            + KEY_ENTREPRENEUR_ID + " TEXT PRIMARY KEY," + KEY_ENTREPRENEUR_NAME + " TEXT," + KEY_ENTREPRENEUR_LOCATION + " TEXT," + KEY_ENTREPRENEUR_PICTURE + " TEXT" + ")";

    private static final String CREATE_TABLE_CLIENT_BASKET = "CREATE TABLE " + TABLE_CLIENT_BASKET + "("+
            KEY_BASKET_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"+ KEY_ID + " TEXT," + KEY_BASKET_QUANTITY + " REAL," + KEY_ENTREPRENEUR_ID + " TEXT" + ")";

    private static final String CREATE_TABLE_CLIENT_CATEGORIES = "CREATE TABLE " + TABLE_CLIENT_CATEGORIES + "("
            + KEY_CATEGORY_ID + " TEXT PRIMARY KEY," + KEY_CATEGORY_NAME + " TEXT" + ")";

    private static final String CREATE_TABLE_CLIENT_ORDERS="CREATE TABLE " + TABLE_CLIENT_ORDERS + "("
            + KEY_ORDER_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_ENTREPRENEUR_ID + " TEXT," + KEY_IS_ORDERED + " BOOLEAN" + ")";

    private static final String CREATE_TABLE_CLIENT_ORDER_DETAILS="CREATE TABLE " + TABLE_CLIENT_ORDER_DETAILS + "("
            + KEY_ORDER_DETAILS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_ORDER_ID + " INTEGER" + KEY_ID + " TEXT," + KEY_ORDER_QUANTITY
            + " FLOAT" + KEY_CUSTOMER_ID + " TEXT" + ")";

    private static final String CREATE_TABLE_DB_STATUS = "CREATE TABLE " + TABLE_DB_STATUS + "("
            + KEY_DB_VERSION + " INTEGER PRIMARY KEY," + KEY_ARTICLE_TABLE_VERSION + " INTEGER," + KEY_CATEGORIES_TABLE_VERSION + " INTEGER," + KEY_ENTREPRENEURS_TABLE_VERSION + " INTEGER" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_CLIENT_ARTICLES);
        db.execSQL(CREATE_TABLE_CLIENT_ENTREPRENEURS);
        db.execSQL(CREATE_TABLE_CLIENT_BASKET);
        db.execSQL(CREATE_TABLE_CLIENT_CATEGORIES);
        db.execSQL(CREATE_TABLE_CLIENT_ORDER_DETAILS);
        db.execSQL(CREATE_TABLE_CLIENT_ORDERS);
        db.execSQL(CREATE_TABLE_DB_STATUS);





        addCategory(db, "001", "Food and beverage");
        addCategory(db, "002", "House and garden");
        addCategory(db, "003", "Sport");
        addCategory(db, "004", "Tools");
        addCategory(db, "005", "Hotel");
        addCategory(db, "006", "Travel");

        addDbVersion(db,2,1,1,1);

        Log.d("DATABASE HELPER", "created table with data");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DATABASE HELPER", "db upgraded");

        // for testing purpose
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ARTICLES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ENTREPRENEURS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_BASKET);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ORDER_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ORDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DB_STATUS);


        // Create tables again
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("DATABASE HELPER", "db downgraded");
        // for testing purpose
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ARTICLES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ENTREPRENEURS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_BASKET);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_CATEGORIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ORDER_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENT_ORDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DB_STATUS);

        // Create tables again
        onCreate(db);
    }

    public List<Article> getAllArticles() {

        List<Article> articles = new ArrayList<Article>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ARTICLES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Log.d("DATABASE HELPER", "get all data from db");
                Article article = new Article();
                article.setID(c.getString((c.getColumnIndex(KEY_ID))));
                article.setmName((c.getString(c.getColumnIndex(KEY_NAME))));

                articles.add(article);

            } while (c.moveToNext());
        }
        db.close();

        return articles;
    }

    public List<Article> getArticlesByEntrepreneur(int _id) {

        List<Article> articles = new ArrayList<Article>();
        
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ARTICLES + " WHERE " + KEY_ENTREPRENEUR_ID + " LIKE '%" + _id + "%'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Article article = new Article();
                article.setID(c.getString((c.getColumnIndex(KEY_ID))));
                article.setmName((c.getString(c.getColumnIndex(KEY_NAME))));
                article.setmQuantity((c.getFloat(c.getColumnIndex(KEY_ARTICLE_QUANTITY))));
                article.setmPrice_1((c.getFloat(c.getColumnIndex(KEY_PRICE_1))));
                article.setmArticlePic(c.getString(c.getColumnIndex(KEY_ARTICLE_PICTURE)));
                articles.add(article);

            } while (c.moveToNext());
        }
    db.close();
        return articles;
    }

    public List<Article> getArticlesByCategory(String _id) {

        List<Article> articles = new ArrayList<Article>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ARTICLES + " WHERE " + KEY_CATEGORY_ID + " LIKE '%" + _id + "%'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Article article = new Article();
                article.setID(c.getString((c.getColumnIndex(KEY_ID))));
                article.setmName((c.getString(c.getColumnIndex(KEY_NAME))));

                articles.add(article);

            } while (c.moveToNext());
        }
        db.close();

        return articles;
    }

    public List<Entrepreneur> getAllEntrepreneurs() {

        List<Entrepreneur> entrepreneurs = new ArrayList<Entrepreneur>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ENTREPRENEURS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Entrepreneur entrepreneur = new Entrepreneur();
                entrepreneur.setmIdEntrepreneur(c.getString((c.getColumnIndex(KEY_ENTREPRENEUR_ID))));
                entrepreneur.setmEntrepreneurName((c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_NAME))));
                entrepreneur.setmEntPic((c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_PICTURE))));
                entrepreneur.setEntLocation(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_LOCATION)));
                entrepreneurs.add(entrepreneur);

            } while (c.moveToNext());
        }
        db.close();

        return entrepreneurs;
    }

    public List<Category> getAllCategories() {

        List<Category> categories = new ArrayList<Category>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_CATEGORIES;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

       
        //looping through all rows and adding to list categories
        if (c.moveToFirst()) {
            do {
                Log.d("DATABASE HELPER", "get all categories from db");
                Category category = new Category();
                category.set_mIdCategory(c.getString((c.getColumnIndex(KEY_CATEGORY_ID))));
                category.setmCategoryName((c.getString(c.getColumnIndex(KEY_CATEGORY_NAME))));

                categories.add(category);

            } while (c.moveToNext());
        }
        db.close();

        return categories;
    }

    public void addArticles(String _id, String name, double warehouse, double price_1, double price_2, String id_Entrepreneur, String id_Category,String article_pic) {

        ContentValues values = new ContentValues();
        values.put(KEY_ID, _id);
        values.put(KEY_NAME, name);
        values.put(KEY_ARTICLE_QUANTITY, warehouse);
        values.put(KEY_PRICE_1, price_1);
        values.put(KEY_PRICE_2, price_2);
        values.put(KEY_ENTREPRENEUR_ID, id_Entrepreneur);
        values.put(KEY_CATEGORY_ID, id_Category);
        values.put(KEY_ARTICLE_PICTURE,article_pic);
        SQLiteDatabase db=this.getReadableDatabase();


        // Inserting row
        db.insert(TABLE_CLIENT_ARTICLES, null, values);
    }

    public void addEntrepreneurs(String _id, String name, String location, String entPic) {

        ContentValues values = new ContentValues();
        values.put(KEY_ENTREPRENEUR_ID, _id);
        values.put(KEY_ENTREPRENEUR_NAME, name);
        values.put(KEY_ENTREPRENEUR_LOCATION, location);
        values.put(KEY_ENTREPRENEUR_PICTURE, entPic);
        SQLiteDatabase db=this.getReadableDatabase();

        // Inserting Row
        db.insert(TABLE_CLIENT_ENTREPRENEURS, null, values);
    }

    public void addCategory(SQLiteDatabase db, String _id, String name) {

        Log.d("DATABASE HELPER", "add category");
        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, _id);
        values.put(KEY_CATEGORY_NAME, name);

   
        db.insert(TABLE_CLIENT_CATEGORIES, null, values);
    }
    public void addDbVersion(SQLiteDatabase db, int dbVer, int artVer,int entVer, int catVer) {

        ContentValues values = new ContentValues();

        values.put(KEY_DB_VERSION, dbVer);
        values.put(KEY_ARTICLE_TABLE_VERSION, artVer);
        values.put(KEY_ENTREPRENEURS_TABLE_VERSION, entVer);
        values.put(KEY_CATEGORIES_TABLE_VERSION, catVer);


        db.insert(TABLE_DB_STATUS, null, values);
    }
    
        public void updateDbVer(String colName,int serverVer) {

        ContentValues values = new ContentValues();

        values.put(colName, serverVer);

            SQLiteDatabase db=this.getReadableDatabase();


        db.update(TABLE_DB_STATUS,values,null,null);
    }


    public void deleteDataFromTable(String tableName) {

        SQLiteDatabase db=this.getReadableDatabase();
        Log.d("DROPTABLE", "deleting data from table"+tableName+"");

        db.delete(tableName,null,null);

    }

    public Article getArticleDetails(String _id) {


        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ARTICLES + " WHERE " + KEY_ID + " LIKE '%" + _id + "%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Article article = new Article();

        if (c != null && c.moveToFirst()) {
            article.setID(c.getString(c.getColumnIndex(KEY_ID)));
            article.setmName(c.getString(c.getColumnIndex(KEY_NAME)));
            article.setmWarehouse(c.getDouble(c.getColumnIndex(KEY_ARTICLE_QUANTITY)));
            article.setmPrice_1(c.getFloat(c.getColumnIndex(KEY_PRICE_1)));
            article.setmPrice_2(c.getFloat(c.getColumnIndex(KEY_PRICE_2)));
            article.setmEntrepreneurId(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_ID)));
            article.setmArticlePic(c.getString(c.getColumnIndex(KEY_ARTICLE_PICTURE)));
        }
        db.close();

        return article;
    }
    public DbVersionCheck getDbVer(){

        String selectQuery = "SELECT * FROM " + TABLE_DB_STATUS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        DbVersionCheck dbVerData=new DbVersionCheck();


        if (c != null && c.moveToFirst()) {
            dbVerData.setmDbVer(c.getInt(c.getColumnIndex(KEY_DB_VERSION)));
            dbVerData.setmArtVer(c.getInt(c.getColumnIndex(KEY_ARTICLE_TABLE_VERSION)));
            dbVerData.setmEntVer(c.getInt(c.getColumnIndex(KEY_ENTREPRENEURS_TABLE_VERSION)));
            dbVerData.setmCatVer(c.getInt(c.getColumnIndex(KEY_CATEGORIES_TABLE_VERSION)));

        }

        db.close();
       return dbVerData;
    }
    public Entrepreneur getEntrepreneurDetails(String _id_entrepreneur) {


        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_ENTREPRENEURS + " WHERE " + KEY_ENTREPRENEUR_ID + " LIKE '%" + _id_entrepreneur + "%'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Entrepreneur entrepreneur = new Entrepreneur();

        Log.d("DATABASE HELPER", "get all data for article");
        if (c != null && c.moveToFirst()) {
            entrepreneur.setmEntrepreneurName(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_NAME)));
            entrepreneur.setmEntPic(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_PICTURE)));
            entrepreneur.setmIdEntrepreneur(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_ID)));
            entrepreneur.setEntLocation(c.getString(c.getColumnIndex(KEY_ENTREPRENEUR_LOCATION)));

        }
        db.close();

        return entrepreneur;
    }

    public void insertIntoBasket( String _id, float quantity, String entrepreneurId) {

        Log.d("DATABASE HELPER", "add to the basket");
        ContentValues values = new ContentValues();
        values.put(KEY_ID, _id);
        values.put(KEY_BASKET_QUANTITY, quantity);
        values.put(KEY_ENTREPRENEUR_ID, entrepreneurId);
        SQLiteDatabase db=this.getReadableDatabase();
        db.insert(TABLE_CLIENT_BASKET, null, values);



    }
    public void updateDbAfterBasket(String id, float newQuantity) {

        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ARTICLE_QUANTITY, newQuantity);

        db.update(TABLE_CLIENT_ARTICLES,values,KEY_ID +"="+id,null);
    }

    public List<Article> getBasketArticles() {

        List<Article> articles = new ArrayList<Article>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLIENT_BASKET+" INNER JOIN "+TABLE_CLIENT_ARTICLES+ " ON "+ TABLE_CLIENT_BASKET+"."+KEY_ID +"="+ TABLE_CLIENT_ARTICLES+"."+KEY_ID ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Log.d("DATABASE HELPER", "get basket items");
                Article article = new Article();
                article.setID(c.getString((c.getColumnIndex(KEY_ID))));
                article.setmName(c.getString(c.getColumnIndex(KEY_NAME)));
                article.setmPrice_1(c.getFloat(c.getColumnIndex(KEY_PRICE_1)));
                article.setmQuantity(c.getFloat(c.getColumnIndex(KEY_BASKET_QUANTITY)));
                article.setmArticlePic(c.getString(c.getColumnIndex(KEY_ARTICLE_PICTURE)));
                articles.add(article);

            } while (c.moveToNext());
        }
        db.close();

        return articles;
    }
    public void deleteBasketRecords(){

        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_CLIENT_BASKET,null,null);

        db.close();
    }
}