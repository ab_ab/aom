package com.cr3dit.alen.aom.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.activity.LoginActivity;
import com.cr3dit.alen.aom.controller.BasketAdapter;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.model.Article;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by alen on 15.01.2017..
 */
public class BasketFragment extends Fragment {
    private static final int CONNECTION_TIMEOUT = 10000;
    private static final int READ_TIMEOUT = 15000;
    CardView checkoutBtn,clearOrderBtn;
    LinearLayout linearLayoutBtnPlaceholder;
    TextView statusText,orderValue;
   private DatabaseHelper mDbHelper;

    private double mTotalSum;
    SharedPreferences accountPref;

    private List<Article> mDataCollection;

    public static BasketFragment newInstance() {
        return new BasketFragment();
    }

    public BasketFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BasketAdapter adapter;
        int articleSize;
        int counter;
        View basketView = inflater.inflate(R.layout.fragment_basket, container, false);

        mDbHelper = new DatabaseHelper(getActivity());

        mDataCollection = mDbHelper.getBasketArticles();
        RecyclerView menuRecycler = (RecyclerView) basketView.findViewById(R.id.recycler_view);
        accountPref = getActivity().getSharedPreferences("MyPref", 0);


        articleSize = mDataCollection.size();

        final String[] articleIds = new String[articleSize];
        final String[] articleNames = new String[articleSize];
        final float[] articlePrice=new float[articleSize];
        final float[] articleQuantity=new float[articleSize];
        final String[] articlePicture=new String[articleSize];
        counter = 0;
        for (Article articles : mDataCollection) {
            articleNames[counter] = articles.getmName();
            articleIds[counter] = articles.getID();
            articlePrice[counter]=articles.getmPrice_1();
            articleQuantity[counter]=articles.getmQuantity();
            articlePicture[counter]=articles.getmArticlePic();
            mTotalSum =(articlePrice[counter]*articleQuantity[counter])+ mTotalSum;
            counter++;
        }

        adapter = new BasketAdapter(getActivity(),articleNames,articlePrice,articleQuantity,articlePicture);

        menuRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        menuRecycler.setLayoutManager(layoutManager);


            return basketView;
    }


    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        clearOrderBtn= (CardView) getView().findViewById(R.id.cv_clearOrderBtn);
        checkoutBtn=(CardView)getView().findViewById(R.id.cv_checkoutBtn);
        linearLayoutBtnPlaceholder=(LinearLayout)getView().findViewById(R.id.linearLayoutBtnPlaceholder);
        statusText=(TextView)getView().findViewById(R.id.textViewStatus);
        orderValue=(TextView)getView().findViewById(R.id.txt_order_value);
        orderValue.setText("Total order: "+String.format("%.2f", mTotalSum)+" KM");

        if(mDataCollection.isEmpty()) {
            linearLayoutBtnPlaceholder.setVisibility(View.GONE);
            statusText.setVisibility(View.VISIBLE);
            orderValue.setVisibility(View.GONE);
            statusText.bringToFront();
        }
        else{
            linearLayoutBtnPlaceholder.setVisibility(View.VISIBLE);
            orderValue.setVisibility(View.VISIBLE);

            statusText.setVisibility(View.GONE);
     }

        clearOrderBtn.setOnClickListener(new View.OnClickListener()

                                       {
                                           @Override
                                           public void onClick(View v) {

                                               mDbHelper.deleteBasketRecords();

                                               FragmentTransaction tr = getFragmentManager().beginTransaction();
                                               tr.replace(R.id.MainFrame, new BasketFragment());
                                               tr.commit();
                                           }
                                       }

        );

        checkoutBtn.setOnClickListener(new View.OnClickListener()

                                       {
                                           @Override
                                           public void onClick(View v) {
                                              if( accountPref.getBoolean("isLoggedIn",false)) {

                                                  AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                                                  builder1.setMessage("Do you want to finish order");
                                                  builder1.setCancelable(true);

                                                  builder1.setPositiveButton(
                                                          "Yes",
                                                          new DialogInterface.OnClickListener() {
                                                              public void onClick(DialogInterface dialog, int id) {
                                                                  dialog.cancel();

                                                                  String json = new Gson().toJson(mDataCollection);
                                                                  Log.d("GET JSON FROMDB", json);

                                                                  new AsyncLogin().execute(json);

                                                                  FragmentTransaction tr = getFragmentManager().beginTransaction();
                                                                  tr.replace(R.id.MainFrame, new BasketFragment());
                                                                  tr.commit();
                                                                  Toast.makeText(getActivity(),"You have successfully sent order.",Toast.LENGTH_SHORT).show();

                                                              }
                                                          });

                                                  builder1.setNegativeButton(
                                                          "No",
                                                          new DialogInterface.OnClickListener() {
                                                              public void onClick(DialogInterface dialog, int id) {
                                                                  dialog.cancel();
                                                              }
                                                          });

                                                  AlertDialog alert11 = builder1.create();
                                                  alert11.show();


                                              }
                                              else {
                                                  Toast.makeText(getActivity(),"You need to log in to finish order.",Toast.LENGTH_SHORT).show();

                                                  startActivity(new Intent(getActivity(), LoginActivity.class).putExtra("fromActivity",false));

                                              }
                                           }

                                       }

        );

    }

    private class AsyncLogin extends AsyncTask<String, String, String> {
        HttpURLConnection conn;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //this method will be running on UI thread
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                url = new URL("http://aomsys.ddns.net/order");

            } catch (MalformedURLException e) {

                e.printStackTrace();
                return "exception";
            }
            try {

                // Setup HttpURLConnection class to send and receive data from php and mysql
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIMEOUT);
                conn.setConnectTimeout(CONNECTION_TIMEOUT);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");

                // setDoInput and setDoOutput method depict handling of both send and receive
                conn.setDoInput(true);
                conn.setDoOutput(true);

                // Append parameters to URL

                Log.d("query holds", params[0]);

                // Open connection for sending data
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(params[0]);
                writer.flush();
                writer.close();
                os.close();
                conn.connect();

            } catch (IOException e1) {

                e1.printStackTrace();
                return "exception";
            }

            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    Toast.makeText(getActivity(),"Your order is sent.",Toast.LENGTH_SHORT).show();

                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            mDbHelper.deleteBasketRecords();

            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.replace(R.id.MainFrame, new BasketFragment());
            tr.commit();

            if (result.equalsIgnoreCase("true")) {
                /* Here launching another activity when login successful. If you persist login state
                use sharedPreferences of Android. and logout button to clear sharedPreferences.
                 */


            } else if (result.equalsIgnoreCase("false")) {

                // If username and password does not match display a error message

            } else if (result.equalsIgnoreCase("exception") || result.equalsIgnoreCase("unsuccessful")) {


            }
        }

    }

}