package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.cr3dit.alen.aom.model.DbVersionCheck;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alen on 22.04.2017..
 */

public class AsyncDbTask extends AsyncTask<String, Void, String> {

    private Context mContext;
     public AsyncResponse delegate=null;

    private int mArtVerServer, mEntVerServer;

     public AsyncDbTask(Context context){
        mContext = context;
    }
    

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
         DatabaseHelper dbHelper;
         DbVersionCheck dbVersionCheckObject;
        int catVerServer;
        String param[]=new String[params.length];
        JSONArray jsonArray= null;
        int artVerLocal,catVerLocal,entVerLocal;

        dbHelper=new DatabaseHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        JSONCreator parser = new JSONCreator();
        dbVersionCheckObject =dbHelper.getDbVer();

        boolean updateArticle=false;
        boolean updateCategories=false;
        boolean updateEntrepreneurs=false;


        for(int x=0;x<params.length;x++)
            {
             param[x] = parser.jsonData(params[x]);
             try {
                    jsonArray = new JSONArray(param[x]);
                    JSONObject jsonObject;

                    if(x==0)
                        {
                        jsonObject = jsonArray.getJSONObject(0);

                        mArtVerServer =Integer.parseInt(jsonObject.getString("article_version"));
                        mEntVerServer =Integer.parseInt(jsonObject.getString("entrepreneur_version"));
                        catVerServer=Integer.parseInt(jsonObject.getString("categories_version"));

                        artVerLocal=dbVersionCheckObject.getmArtVer();
                        entVerLocal=dbVersionCheckObject.getmEntVer();
                        catVerLocal=dbVersionCheckObject.getmCatVer();

                        if (artVerLocal != mArtVerServer)
                            {
                            updateArticle = true;
                            }
                        if(entVerLocal!= mEntVerServer)
                            {
                            updateEntrepreneurs=true;
                            }
                        if(catVerLocal!=catVerServer)
                            {
                            updateCategories=true;
                            }
                        }
                    else if(x==1&&updateArticle) 
                        {
                        dbHelper.deleteDataFromTable("articles");

                        for (int y = 0; y < jsonArray.length(); y++) 
                            {
                            jsonObject = jsonArray.getJSONObject(y);
                            dbHelper.addArticles(jsonObject.getString("_article_id"),jsonObject.getString("article_name"),0,Double.parseDouble(jsonObject.getString("article_price1")),0,jsonObject.getString("_entrepreneur_id"),jsonObject.getString("_category_id"),jsonObject.getString("article_picture"));
                            }
                        dbHelper.updateDbVer("articles_version", mArtVerServer);
                        updateArticle=false;                                     
                        }
                    else if(x==2&&updateEntrepreneurs)
                        {
                        dbHelper.deleteDataFromTable("entrepreneurs");

                        for (int y = 0; y < jsonArray.length(); y++) 
                            {
                            jsonObject = jsonArray.getJSONObject(y);
                            dbHelper.addEntrepreneurs(jsonObject.getString("_entrepreneur_id"),jsonObject.getString("entrepreneur_name"),jsonObject.getString("entrepreneur_location"),jsonObject.getString("entrepreneur_picture"));
                            }
                        dbHelper.updateDbVer("entrepreneurs_version", mEntVerServer);
                        }
                    else if(x==3&&updateCategories)
                        {
                        for (int y = 0; y < jsonArray.length(); y++) 
                            {
                            jsonObject = jsonArray.getJSONObject(y);
                            }
                        }

                }
            catch (JSONException e) 
                {
                    e.printStackTrace();
                }
            }
            return "result";
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        //delegated result to processFinish method of interface
        delegate.processFinish(response);
    }
}

