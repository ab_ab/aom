package com.cr3dit.alen.aom.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.model.Article;
import com.squareup.picasso.Picasso;

/**
 * Created by alen on 15.01.2017..
 */
public class ArticleDetailFragment extends Fragment {


    private String mArticleId ="";
    private Article mArticle;
    private float mUpdateQuantity;
    private double mBasePrice;
    private DatabaseHelper mDbHelper;
    TextView articleName,articlePrice,quantity;
    ImageView articlePicture;

    CardView cv_addToBasketButton, cv_increment,cv_decrement;

    public ArticleDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences pref;
        mDbHelper =new DatabaseHelper(getActivity());
        pref = getActivity().getPreferences(0);
        mArticleId = pref.getString("ArticleClicked", "notClicked");
    return inflater.inflate(R.layout.fragment_detail,container,false);

    }

     public void onActivityCreated(final Bundle savedInstanceState){
         super.onActivityCreated(savedInstanceState);
         String URL="http://aomsys.ddns.net";

         try {
             mArticle = mDbHelper.getArticleDetails(mArticleId);

         }
         catch (Exception e){e.printStackTrace();}
       getUiElements();

         mBasePrice = mArticle.getmPrice_1();

         articleName.setText(mArticle.getmName());
         articlePrice.setText(String.format("%.2f", mBasePrice));

            URL=URL+ mArticle.getmArticlePic();

            Log.d("IMGAREURL", URL+"");
articlePicture.setImageResource(R.drawable.no_img);
         if (!mArticle.getmArticlePic().isEmpty()){

             Picasso.with(getContext()).load(URL).fit().centerCrop().into(articlePicture);
         }




         mUpdateQuantity =Float.valueOf(quantity.getText().toString());


         cv_increment.setOnClickListener(new View.OnClickListener(){
             @Override
             public void onClick(View v) {
                 mUpdateQuantity = mUpdateQuantity +1;
                quantity.setText(mUpdateQuantity +"");



                 articlePrice.setText( String.format("%.2f", mBasePrice * mUpdateQuantity)+" KM");
             }
         });

         cv_decrement.setOnClickListener(new View.OnClickListener(){
             @Override
             public void onClick(View v) {
                 if (mUpdateQuantity >1){
                 mUpdateQuantity = mUpdateQuantity -1;
                 quantity.setText(mUpdateQuantity +"");


                     articlePrice.setText( String.format("%.2f", mBasePrice * mUpdateQuantity)+" KM");

                 }}
         });


        cv_addToBasketButton.setOnClickListener(new View.OnClickListener(){
    @Override
    public void onClick(View v) {
        Toast.makeText(getActivity(),"Article added to basket",Toast.LENGTH_SHORT).show();
        mUpdateQuantity =Float.valueOf(quantity.getText().toString());

        mDbHelper.insertIntoBasket(mArticle.getID(), mUpdateQuantity,"0001");


    }
});


    }
    public void getUiElements(){

        articleName=(TextView)getView().findViewById(R.id.txt_articleName);
        articlePrice=(TextView)getView().findViewById(R.id.txt_articlePrice);
        quantity=(TextView)getView().findViewById(R.id.txt_quantity);
articlePicture=(ImageView)getView().findViewById(R.id.img_entLogo);
        cv_addToBasketButton =(CardView)getView().findViewById(R.id.cv_addToBasketButton);
        cv_increment=(CardView)getView().findViewById(R.id.cv_increment);
        cv_decrement=(CardView)getView().findViewById(R.id.cv_decrement);


    }


}

