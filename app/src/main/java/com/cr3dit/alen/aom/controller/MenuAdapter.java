package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.squareup.picasso.Picasso;


/**
 * Created by alen on 08.01.2017..
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
   private Context mContext;
    private String[] mMenuItems;
    private String[] mMenuPic;
    private String[] mMenuLocation;
    private Listener mListener;
    final private String URL="http://aomsys.ddns.net/";



    public interface Listener {
        void onClick(int position);
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    //constructor for 1 variable


    //constructor for 2 variables
    public MenuAdapter(Context context, String[] menuItems,String[] menuPic, String[] menuLocation) {
        this.mMenuPic = menuPic;
        this.mMenuItems = menuItems;
        this.mMenuLocation =menuLocation;
        this.mContext =context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_entrepreneur_detail, parent, false);
        return new ViewHolder(cv);
    }


    public void onBindViewHolder(ViewHolder holder, final int position) {

        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        ImageView imageView=(ImageView) cardView.findViewById(R.id.img_entLogo);
        TextView txtLocation=(TextView)cardView.findViewById(R.id.txt_location);
        textView.setText(mMenuItems[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        txtLocation.setText(mMenuLocation[position]);
       // imageView.setImageResource(mMenuPic[position]);

        Picasso.with(mContext).load(URL+ mMenuPic[position]).into(imageView);
        Log.d("IMAGEURL", URL+ mMenuPic[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return mMenuItems.length;
    }
}