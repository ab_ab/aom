package com.cr3dit.alen.aom.controller;


import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by alen on 24.04.2017..
 */

 class JSONCreator {


     String jsonData(String passedUrl) {
        BufferedReader bufferedReader = null;
        String json;
        try {
            URL url = new URL(passedUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            StringBuilder sb = new StringBuilder();
             int response_code = con.getResponseCode();
            if (response_code == HttpURLConnection.HTTP_OK) {
            //HTTP_OK=200
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            Log.d("RESPONSEHTTP",response_code+"");
            while ((json = bufferedReader.readLine()) != null)
            {
                sb.append(json + "\n");
            }
            return sb.toString().trim();

                }
            return "err";

        }
        catch (Exception e)
        {
            return null;
        }
    }
}