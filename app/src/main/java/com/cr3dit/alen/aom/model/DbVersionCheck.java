package com.cr3dit.alen.aom.model;

/**
 * Created by alen on 22.04.2017..
 */

public class DbVersionCheck {
    private int mDbVer, mArtVer, mEntVer, mCatVer;

    public int getmDbVer() {
        return mDbVer;
    }

    public void setmDbVer(int mDbVer) {
        this.mDbVer = mDbVer;
    }

    public int getmArtVer() {
        return mArtVer;
    }

    public void setmArtVer(int mArtVer) {
        this.mArtVer = mArtVer;
    }

    public int getmEntVer() {
        return mEntVer;
    }

    public void setmEntVer(int mEntVer) {
        this.mEntVer = mEntVer;
    }

    public int getmCatVer() {
        return mCatVer;
    }

    public void setmCatVer(int mCatVer) {
        this.mCatVer = mCatVer;
    }

    public DbVersionCheck(){};



}
