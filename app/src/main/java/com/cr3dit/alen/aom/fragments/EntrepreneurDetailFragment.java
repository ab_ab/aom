package com.cr3dit.alen.aom.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.cr3dit.alen.aom.controller.ArticlesAdapter;
import com.cr3dit.alen.aom.controller.DatabaseHelper;
import com.cr3dit.alen.aom.model.Article;
import com.cr3dit.alen.aom.model.Entrepreneur;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by alen on 10.01.2017..
 */

public class EntrepreneurDetailFragment extends Fragment {
    private DatabaseHelper mDbHelper;
    private String mEntrepreneur_clicked;
    private List<Article> mDataCollection;
    Entrepreneur entrepreneur;
    SharedPreferences pref;

    ArticlesAdapter adapter;
    TextView entName,statusText,entLocation;
    ImageView entLogo;
    String URL="http://aomsys.ddns.net/";


    public EntrepreneurDetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDbHelper = new DatabaseHelper(getActivity());
        int articleSize;
        int counter;
        View entFrag = inflater.inflate(R.layout.fragment_ent_detail, container, false);
        RecyclerView menuRecycler = (RecyclerView) entFrag.findViewById(R.id.recycler_view);

        pref = getActivity().getPreferences(0);
        mEntrepreneur_clicked = pref.getString("EntrepreneurClicked", "notClicked");
        Log.d("ENTREPRENEUR CLICKED", mEntrepreneur_clicked);

        mDataCollection = mDbHelper.getArticlesByEntrepreneur(Integer.parseInt(mEntrepreneur_clicked));

       // pref.edit().remove("EntrepreneurClicked").apply();

        articleSize = mDataCollection.size();

        final String[] articleIds = new String[articleSize];
        final String[] articleNames = new String[articleSize];
        final String[] articlePic = new String[articleSize];

        final float[] articlePrice=new float[articleSize];
        counter = 0;
        for (Article articles : mDataCollection) {
            articleNames[counter] = articles.getmName();
            articleIds[counter] = articles.getID();
            articlePrice[counter]=articles.getmPrice_1();
            articlePic[counter] = articles.getmArticlePic();

            counter++;
        }

        final FragmentTransaction ft = getFragmentManager().beginTransaction();

        adapter = new ArticlesAdapter(getActivity(),articleNames,articlePrice,articlePic);

        menuRecycler.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        menuRecycler.setLayoutManager(layoutManager);


        adapter.setListener(new ArticlesAdapter.Listener() {
            public void onClick(int position) {

                pref = getActivity().getPreferences(0);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("ArticleClicked", (articleIds[position]));
                editor.apply();
                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
                ft.addToBackStack(null);
                ft.replace(R.id.MainFrame, new ArticleDetailFragment(), "NewFragmentTag");
                ft.commit();

            }
        });

        return entFrag;

    }

    @Override
    public void onResume() {
        super.onResume();
        Picasso.with(getActivity()).load(URL + entrepreneur.getmEntPic()).into(entLogo);


    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        entrepreneur = mDbHelper.getEntrepreneurDetails(mEntrepreneur_clicked);

        getUiElements();
        entName.setText(entrepreneur.getmEntrepreneurName());
        entLocation.setText(entrepreneur.getEntLocation());



        if(mDataCollection.isEmpty()) {
            statusText.setVisibility(View.VISIBLE);
            statusText.bringToFront();
        }
        else{
            statusText.setVisibility(View.GONE);
        }


    }



    public void getUiElements() {

        entName = (TextView) getView().findViewById(R.id.txt_articleName);
        entLogo = (ImageView) getView().findViewById(R.id.img_entLogo);
        statusText=(TextView)getView().findViewById(R.id.textViewStatus);
entLocation=(TextView)getView().findViewById(R.id.txt_details);
    }


}