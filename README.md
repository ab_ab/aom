# README #

Android information system for order management

### What is this repository for? ###

* Online representation of Masters program	
* Version control
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


### ER Class diagram (outdated) ###

Changes are needed 
![class ER diagram.jpg](https://bitbucket.org/repo/jeKzgy/images/3274965547-class%20ER%20diagram.jpg)

### AOM ER database diagram ###
![![AOM DB ERD.png](https://bitbucket.org/repo/jeKzgy/images/3434412524-AOM%20DB%20ERD.png)](https://bitbucket.org/repo/jeKzgy/images/2168639295-AOM%20DB%20ERD.png)