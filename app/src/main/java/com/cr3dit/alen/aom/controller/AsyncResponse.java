package com.cr3dit.alen.aom.controller;


/**
 * Created by alen on 22.04.2017..
 */

public interface AsyncResponse{
    void processFinish(String stringResponse);
}
