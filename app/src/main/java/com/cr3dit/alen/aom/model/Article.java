package com.cr3dit.alen.aom.model;


/**
 * Created by alen on 12.01.2017..
 */

public class Article {

    //private variables
    private String mId,mName,mEntrepreneurId,mArticlePic;
    private double mWarehouse;
    private float mPrice_1, mPrice_2, mQuantity;


    public Article() {

    }

    public String getmArticlePic() {
        return mArticlePic;
    }

    public void setmArticlePic(String mArticlePic) {
        this.mArticlePic = mArticlePic;
    }







    public String getmEntrepreneurId() {
        return mEntrepreneurId;
    }

    public void setmEntrepreneurId(String mEntrepreneurId) {
        this.mEntrepreneurId = mEntrepreneurId;
    }


    public double getmWarehouse() {
        return mWarehouse;
    }

    public void setmWarehouse(double mWarehouse) {
        this.mWarehouse = mWarehouse;
    }

    public float getmPrice_1() {
        return mPrice_1;
    }

    public void setmPrice_1(float mPrice_1) {
        this.mPrice_1 = mPrice_1;
    }

    public float getmPrice_2() {
        return mPrice_2;
    }

    public void setmPrice_2(float mPrice_2) {
        this.mPrice_2 = mPrice_2;
    }

    // getting mName
    public String getmName() {
        return this.mName;
    }

    // setting mName
    public void setmName(String mName) {
        this.mName = mName;
    }

    // getting ID
    public String getID() {

        return this.mId;
    }

    // setting ID
    public void setID(String _id) {

        this.mId = _id;
    }


    public void setmQuantity(float mQuantity) {
        this.mQuantity = mQuantity;
    }

    public float getmQuantity() {
        return mQuantity;
    }
}