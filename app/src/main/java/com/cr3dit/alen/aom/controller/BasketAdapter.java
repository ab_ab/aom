package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.squareup.picasso.Picasso;


/**
 * Created by alen on 08.01.2017..
 */

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder> {

    private String[] mArticleNames;
    private float[] mPrice;
    private float[] mQuantity;
    private String[] mArticlePic;
    private Context mContext;


    private Listener mListener;

    interface Listener {
        void onClick(int position);

    }
    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    //constructor for 2 variables
    public BasketAdapter(Context context,String[] articleNames, float[] price,float[]quantity,String[]articlePic) {
        this.mArticleNames = articleNames;
        this.mPrice = price;
        this.mQuantity =quantity;
        this.mArticlePic =articlePic;
        this.mContext =context;

    }

     class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

         ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_placeholder_basket_articles, parent, false);
        return new ViewHolder(cv);
    }

    public void onBindViewHolder(ViewHolder holder, final int position) {
        String URL="http://aomsys.ddns.net/";
        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        textView.setText(mArticleNames[position]);

        TextView textView2 = (TextView) cardView.findViewById(R.id.txt_price);
        textView2.setText(String.format("%.2f", mPrice[position]* mQuantity[position])+" KM");

        TextView txtQuantity=(TextView)cardView.findViewById(R.id.txt_quantity);
        txtQuantity.setText(String.format("%.0f", mQuantity[position])+" PCS");
        ImageView imageView=(ImageView)cardView.findViewById(R.id.imageView);

        imageView.setImageResource(R.drawable.no_img);

        if (!mArticlePic[position].isEmpty()){
            Picasso.with(mContext).load(URL + mArticlePic[position]).into(imageView);
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position);


                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return mArticleNames.length;
    }
}