package com.cr3dit.alen.aom.controller;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cr3dit.alen.aom.R;
import com.squareup.picasso.Picasso;


/**
 * Created by alen on 08.01.2017..
 */

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private String[] mArticleNames;
    private float[] mPrice;
    private String[] mArticlePic;
    private Context mContext;

    private Listener mListener;

    public interface Listener {
        void onClick(int position);


    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    //constructor for 2 variables
    public ArticlesAdapter(Context context,String[] articleNames, float[] price,String[] articlePic) {
        this.mArticleNames = articleNames;
        this.mPrice = price;
        this.mArticlePic =articlePic;
        this.mContext =context;

    }


     class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_placeholder_articles, parent, false);
        return new ViewHolder(cv);
    }


    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        TextView textView = (TextView) cardView.findViewById(R.id.txt_articleName);
        ImageView imageView=(ImageView) cardView.findViewById(R.id.imageView);

        textView.setText(mArticleNames[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        final TextView textView2 = (TextView) cardView.findViewById(R.id.txt_price);
        textView2.setText(mPrice[position]+" KM");

        imageView.setImageResource(R.drawable.no_img);

        if (!mArticlePic[position].isEmpty()){
            String URL = "http://aomsys.ddns.net/";
            Picasso.with(mContext).load(URL + mArticlePic[position]).into(imageView);
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(position);


                }
            }
        });
    }

    @Override
    public int getItemCount() {

        return mArticleNames.length;
    }
}