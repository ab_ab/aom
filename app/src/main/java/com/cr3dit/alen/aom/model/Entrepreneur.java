package com.cr3dit.alen.aom.model;

/**
 * Created by alen on 19.01.2017..
 */

public class Entrepreneur {
    private String mIdEntrepreneur, mEntrepreneurName, mEntPic;


    public String getEntLocation() {
        return entLocation;
    }

    public void setEntLocation(String entLocation) {
        this.entLocation = entLocation;
    }

    String entLocation;
    public Entrepreneur() {

    }

    public String getmIdEntrepreneur() {
        return mIdEntrepreneur;
    }

    public void setmIdEntrepreneur(String mIdEntrepreneur) {
        this.mIdEntrepreneur = mIdEntrepreneur;
    }
    public void setmEntPic(String mEntPic){

        this.mEntPic = mEntPic;
    }
    public String getmEntPic(){return mEntPic;}

    public String getmEntrepreneurName() {
        return mEntrepreneurName;
    }

    public void setmEntrepreneurName(String mEntrepreneurName) {
        this.mEntrepreneurName = mEntrepreneurName;
    }
}
