package com.cr3dit.alen.aom.model;


/**
 * Created by alen on 22.01.2017..
 */

public class Category {
    private String _mIdCategory, mCategoryName;

    public Category() {

    }

    public String getmCategoryName() {
        return mCategoryName;
    }

    public void setmCategoryName(String mCategoryName) {
        this.mCategoryName = mCategoryName;
    }

    public String get_mIdCategory() {
        return _mIdCategory;
    }

    public void set_mIdCategory(String _mIdCategory) {
        this._mIdCategory = _mIdCategory;
    }
}



